# Tensions

## Definition

A tension is defined as the  distance between where you are and where you want to be. A tension can be used for example to track and keep record of tasks, issues or ideas. A tension, like an email, has an emitter and a receiver (a role or a circle).

A tension belong to a type according to its topic. There are four different types:

* **Operational**: A general category of tension which can be a task, an issue or whatever that need to be share in the organisation.
* **Governance**: concerning the structure of the organisation its mandates etc.
* **Help**: for help requests, questions or clarifications.
* **Alert**: for alert requests, this are used for global announcement. All the member behind the circle alerted will be notified (Coordinator level to trigger).
