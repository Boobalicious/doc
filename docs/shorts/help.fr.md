## Définitions

**organisation** (fractale) : un groupe d'humains partageant une raison d'être commune (cercle racine) composé de roles et de cercles.

**Role** : une fonction réalisé par un membre avec un mandat donné.   

**Circle** : ensemble de role et de sous-cercles (équipe, groupe etc).  

**Mandat** : document qui définit la fonction d'un role ou d'un cercle en explicitant :

* sa raison d'être : pourquoi il existe
* ses responsabilités (optionel) : quelle sont les attentes de ce role/cercle.
* ses domaines (optionel) : quelle propriétés/ressources il contrôle.
* ses politiques (optionel) : les regles d'acces à ses ressources.

**Tension** : distance entre la ou l'on est et là l'on veut aller. Une tension peut être utilisée par exemple pour partager et gérer des tâches, des questions ou des idées. Une tension, comme un courrier électronique, a un émetteur et un destinataire (un rôle ou un cercle).
Une tension appartient à un type en fonction du sujet qu'elle traite. Il y a quatre types différents :

* Opérationnelle : Une catégorie générale de tension qui peut être une tâche, un problème ou tout ce qui doit être partagé dans l'organisation.
* Gouvernance : concernant la structure de l'organisation, ses mandats etc.
* Entraide : pour les demandes d'aide, questions ou clarifications.
* Alerte: Pour les alertes, annonce genéral ou chaque membre sous le cercle alerté recevra une notification (niveau de coordinateur requis pour déclencher).


## Créer une organisation

Cliquez sur le bouton + dans le coin supérieur droit de la page. Cliquez ensuite sur le bouton 'Créer une nouvelle organisation'.

## Processus de gouvernance

Le processus de gouvernance permet de définir la portée des droits associés au roles dans l'organisation. Il existe deux processus de gouvernance :

###### Coordonné

Dans le mode coordoné, les rôles de coordinateur ont les droits suivant au sein de leurs cercles :

* création de rôles et de sous-cercles
* modification des rôles et des sous-cercles
* création de tensions et commentaires
* modification des tensions (titre, labels, assignés)
* déplacement des tensions

Les rôles normaux ont les droits suivante au sein de leurs cercles :

* création de tension et commentaires 
* fermer ses propres tensions 

###### Agile

Dans le mode agile les rôles ont les droits suivant au sein de leurs cercles :

* création de rôles et de sous-cercles
* modification des rôles et des sous-cercles
* création de tension et commentaires
* modification de tension (titre, labels, assignés)
* déplacement des tensions

Note: les droits d'un rôle associé à un cercle sont hérité dans les sous-cercles qui ne possède pas rôles


