# Welcome to Fractale documentation

**Fractale** ([fractale.co](https://fractale.co){:target="_blank"}) is a platform that provides tools and infrastructures to support organisations in the process of self-organisation. That means a shift from pyramidal management with subordination relation towards fractal circles with relationship based on trust and peer to peer commitments.

The platform offers a radical transparency of the structure of the organisation, allows members to communicate through a tensions publishing system, and organisations to evolve. Its goal 
is to facilitate organisations to achieve their goals through mutual aid and collective intelligence.

To do so, the platform focus on three core principles:

* **Transparency**: each member of an organisation is able to see the organisation structure and its components: [circles, roles, mandates](circle), [tensions and journals](tension).
* **Tension-driven** decision making: Each member of the organisation can communicate through [tensions](tension) who can be collectively discussed in order to be solved.
* **Adaptation**: organisations are dynamic and evolve through a dialogue between the current institutions and the tensions. This translates by the ability to [modify the roles and mandates](shorts/help).

Organisations are seen like graphs, or trees, which are akin to living entities and where their members can intuitively visit them, and interact with, in order to:

* quickly understand who is doing what.
* quickly understand what is to be done.
* quickly detect and respond to tensions.
* quickly find the right people to contact to get information.
* facilitate the on-boarding flow of newcomers.
* be robust to growth and reorganization.


It is inspired by several ideas and initiatives [^1]:

* The [Holacracy](https://en.wikipedia.org/wiki/Holacracy){:target="_blank"} and its [constitution](https://www.holacracy.org/constitution)
* The [Sociocracy](https://en.wikipedia.org/wiki/Sociocracy){:target="_blank"}
* The [Teal organisation](https://reinventingorganizationswiki.com){:target="_blank"}
* The [Stigmergy](https://wiki.p2pfoundation.net/Stigmergy){:target="_blank"}
<!--* The [liberated company](https://en.wikipedia.org/wiki/Liberated_company). -->
* Collaborative tool for developer such as [Gitlab](https://en.wikipedia.org/wiki/GitLab){:target="_blank"} and [Github](https://github.com){:target="_blank"}.
* The way living organism are designed, made up of organs that are simultaneously interdependent *and* autonomous but act towards a common purpose and capable of adaptation according to inner and outer tensions.

[^1]: But note that we do not pretend to conform to any of this solution and rather aims to be adaptable to a plurality of approaches.

